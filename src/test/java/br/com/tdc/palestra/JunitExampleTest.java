package br.com.tdc.palestra;

import static org.junit.Assert.*;

import org.junit.Test;

public class JunitExampleTest {

	@Test
	public void testAssertEquals() {
		JunitExample expected = new JunitExample();
		JunitExample actual = new JunitExample();

		assertEquals(expected, actual);
	}

	@Test
	public void testAssertNotEquals() {
		JunitExample expected = new JunitExample();
		JunitExample actual = new JunitExample();

		assertNotEquals(expected, actual);
	}

	@Test
	public void testAssertSame() {
		JunitExample expected = new JunitExample();
		JunitExample actual = expected;

		assertSame(expected, actual);
	}

	@Test
	public void testAssertNotSame() {
		JunitExample expected = new JunitExample();
		JunitExample actual = new JunitExample();

		assertNotSame(expected, actual);
	}

	@Test
	public void testAssetNull() {
		JunitExample example = new JunitExample();
		assertNull(example.getNull());
	}

	@Test
	public void testAsserNotNull() {
		JunitExample example = new JunitExample();
		assertNotNull(example);
	}

	@Test
	public void testAssertTrue() {
		JunitExample example = new JunitExample();
		assertTrue(example.isTrue());
	}

	@Test
	public void testAssertFalse() {
		JunitExample example = new JunitExample();
		assertFalse(example.isFalse());
	}

	@Test
	public void testAssertArrayEquals() {
		String expected[] = {"Junit", "Framework", "Assertions"};
		String actual[] = {"Junit", "Framework", "Assertions"};

		assertArrayEquals(expected, actual);
	}

	@Test
	public void testAssertFloatValues() {
		double expected = 1.75;
		double actual = 1.753;

		assertEquals(expected, actual, 0.003);
	}

	@Test(expected=UnsupportedOperationException.class)
	public void testException() {
		JunitExample example = new JunitExample();
		example.raiseException();
	}
}
