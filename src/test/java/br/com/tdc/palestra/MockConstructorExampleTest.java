package br.com.tdc.palestra;

import java.time.LocalDate;
import java.time.ZoneOffset;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest(MockConstructorExample.class)
public class MockConstructorExampleTest {

	@Test
	public void testMockConstructor() throws Exception {
		LocalDate date = LocalDate.of(2016, 01, 01);
		Date expected = Date.from(date.atStartOfDay(ZoneOffset.UTC).toInstant());

		PowerMockito.whenNew(Date.class).withNoArguments().thenReturn(expected);

		MockConstructorExample example = new MockConstructorExample();
		assertEquals(expected, example.date());
	}
}
