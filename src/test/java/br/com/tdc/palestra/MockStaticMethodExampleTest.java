package br.com.tdc.palestra;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest(MockStaticMethodExample.class)
public class MockStaticMethodExampleTest {

	@Test
	public void testMockStaticMethod() {
		LocalDate expected = LocalDate.of(2017, 01, 01);

		PowerMockito.mockStatic(LocalDate.class);
		Mockito.when(LocalDate.now()).thenReturn(expected);

		MockStaticMethodExample example = new MockStaticMethodExample();
		assertEquals(expected, example.date());
	}
}
