package br.com.tdc.palestra;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.util.reflection.Whitebox;

public class MockPrivateFieldExampleTest {

	@Mock
	private Investimento investimentoMock;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testPrivateField() {
		int expectedDays = 90;
		Mockito.when(investimentoMock.numeroDeDias()).thenReturn(expectedDays);

		MockPrivateFieldExample example = new MockPrivateFieldExample();
		Whitebox.setInternalState(example, "investimento", investimentoMock);
		assertEquals(expectedDays, example.getDays());
	}
}
