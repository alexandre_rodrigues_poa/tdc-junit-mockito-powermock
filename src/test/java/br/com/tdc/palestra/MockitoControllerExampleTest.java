package br.com.tdc.palestra;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class MockitoControllerExampleTest {

	@Mock
	private EmailService emailServiceMock;

	@InjectMocks
	private MockitoControllerExample controller;

	@Test
	public void testSendEmail() {
		//Verificar chamada de metodos sem retorno
		controller.sendEmail();
		verify(emailServiceMock, times(1)).send();
	}
}
