package br.com.tdc.palestra;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyObject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class MockitoExampleTest {

	private final double alicota = 50;
	private final double lucro = 1000;

	@Mock
	private Investimento investimentoMock;

	@Test
	public void testRetornoDeFuncoes() {
		IOF iofMock = Mockito.mock(IOF.class);

		Mockito.when(investimentoMock.lucro()).thenReturn(lucro);
		Mockito.when(iofMock.alicota(anyObject())).thenReturn(alicota);

		double imposto = lucro * (alicota / 100);
		CalculadoraDeImposto calculadora = new CalculadoraDeImposto();

		assertEquals(imposto, calculadora.calcularIof(investimentoMock, iofMock), 0);
	}

	@Test
	public void testRetornoDeFuncaoComParametroDefinido() {
		Investimento investimento = new Investimento();
		IOF iofMock = Mockito.mock(IOF.class);

		Mockito.when(iofMock.alicota(investimento)).thenReturn(alicota);

		double imposto = investimento.lucro() * (alicota / 100);
		CalculadoraDeImposto calculadora = new CalculadoraDeImposto();

		assertEquals(imposto, calculadora.calcularIof(investimento, iofMock), 0);
	}

	@Test(expected=IllegalStateException.class)
	public void testRetornoDeUmaExcecao() {
		IOF iofMock = Mockito.mock(IOF.class);

		Mockito.when(investimentoMock.lucro()).thenReturn(lucro);
		Mockito.doThrow(IllegalStateException.class).when(iofMock).alicota(investimentoMock);

		CalculadoraDeImposto calculadora = new CalculadoraDeImposto();
		calculadora.calcularIof(investimentoMock, iofMock);
	}

	@Test
	public void testOrdemDeChamada() {
		IOF iofMock = Mockito.mock(IOF.class);

		Mockito.when(iofMock.alicota(investimentoMock)).thenReturn(alicota);
		Mockito.when(investimentoMock.lucro()).thenReturn(lucro);

		CalculadoraDeImposto calculadora = new CalculadoraDeImposto();

		calculadora.calcularIof(investimentoMock, iofMock);

		InOrder order = Mockito.inOrder(investimentoMock, iofMock);

		order.verify(iofMock).alicota(investimentoMock); //Primeiro método a ser chamado
		order.verify(investimentoMock).lucro();//Segundo método a ser chamado
	}
}
