package br.com.tdc.palestra;

public class JunitExample {

	public boolean isTrue() {
		return true;
	}

	public boolean isFalse() {
		return false;
	}

	public Object getNull() {
		return null;
	}

	public void raiseException() {
		throw new UnsupportedOperationException();
	}
}
