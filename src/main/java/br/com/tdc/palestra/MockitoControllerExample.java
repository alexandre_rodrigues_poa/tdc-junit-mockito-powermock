package br.com.tdc.palestra;

import javax.inject.Inject;

public class MockitoControllerExample {

	@Inject
	private EmailService email;

	public void sendEmail() {
		email.send();
	}
}
