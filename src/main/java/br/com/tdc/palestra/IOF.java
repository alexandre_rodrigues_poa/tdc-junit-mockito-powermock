package br.com.tdc.palestra;

import java.util.HashMap;
import java.util.Map;

public class IOF {
	private final Map<Integer, Double> percentuais = new HashMap<>();

	public IOF() {
		percentuais.put(1, new Double(96));
		percentuais.put(2, new Double(93));
		percentuais.put(3, new Double(90));
		percentuais.put(4, new Double(86));
		percentuais.put(5, new Double(83));
		percentuais.put(6, new Double(80));
		percentuais.put(7, new Double(76));
		percentuais.put(8, new Double(73));
		percentuais.put(9, new Double(70));
		percentuais.put(10, new Double(66));

		percentuais.put(11, new Double(63));
		percentuais.put(12, new Double(60));
		percentuais.put(13, new Double(56));
		percentuais.put(14, new Double(53));
		percentuais.put(15, new Double(50));
		percentuais.put(16, new Double(46));
		percentuais.put(17, new Double(43));
		percentuais.put(18, new Double(40));
		percentuais.put(19, new Double(36));
		percentuais.put(20, new Double(33));

		percentuais.put(21, new Double(30));
		percentuais.put(22, new Double(26));
		percentuais.put(23, new Double(23));
		percentuais.put(24, new Double(20));
		percentuais.put(25, new Double(16));
		percentuais.put(26, new Double(13));
		percentuais.put(27, new Double(10));
		percentuais.put(28, new Double(06));
		percentuais.put(29, new Double(03));
	}

	public double alicota(Investimento investimento) {
		return percentuais.get(investimento.numeroDeDias());
	}
}
