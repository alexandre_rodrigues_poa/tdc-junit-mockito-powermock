package br.com.tdc.palestra;

import java.time.LocalDate;

public class MockStaticMethodExample {

	public LocalDate date() {
		return LocalDate.now();
	}
}
