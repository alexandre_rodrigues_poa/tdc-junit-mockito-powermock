package br.com.tdc.palestra;

public class CalculadoraDeImposto {

	public double calcularIof(Investimento investimento, IOF iof) {
		double alicota = iof.alicota(investimento);
		double lucro = investimento.lucro();
		return lucro * (alicota / 100);
	}

}
